#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# En esta funcion se obtendra el promedio por medio de:
# sum lo cual nos da la suma total de  todo los numeros
# len lo cual nos da el total de las variables en la lista
def promedio(lista=[]):
    promedio=sum(lista)/len(lista)
    return promedio

# La funcion de los cuadrados se obtedran por medio de:
# Multiplicando la variable lista[a] por si mismo, 'a' ira aumentando
# Ingresandolo a una nueva lista la cual sera "listada"
def cuadrados(lista=[]):
    total=len(lista)
    num=0
    sqnum=[]
    a=0
    for x in range(0, total):
        while a!=total:
            sq=0
            sq=lista[a]*lista[a]
            sqnum.append(sq)
            a=a+1
            pass
    print("Los numeros al cuadrado son: ", list(sqnum))
    pass

# la funcion de la palabra mas larga se obtendra por medio de:
# Consiguiendo el largo de la palabras con len
# En el caso que supere el mayor, se guardara la posicion de la palabra
def cuenta_letras(palabras=[]):
    mayor=0
    total=len(palabras)
    a=0
    for x in range(0, total):
        largo=len(palabras[a])
        if largo>mayor:
            mayor=largo
            Win=a
            pass
        a=a+1
        pass
    print("La palabra mas larga es:\n", palabras[Win])

# Programa principal
lista=[]
prom=None
sq=None
confirmar=None
# El usuario ingresara valores float hasta que este quiera terminar
while confirmar!='N' and confirmar!='n':
    num=0
    num=float(input("¿Que numero desea agregar a la lista?\n"))
    lista.append(num)
    print("¿Desea agregar otro numero?")
    confirmar=(input("Caso contrario, por favor, ingresar N \n"))
    pass
# La lista creada sera procesada para tener cuadrado y promedio
prom=promedio(lista)
print("El promedio de la lista es de: ", prom)
cuadrados(lista)
palabras=[]
# Confirmar sera reiniciado para comenzar el proceso de los strings
confirmar=None
# El usuario ingresara valores string hasta que este quiera terminar
while confirmar!='N' and confirmar!='n':
    word=None
    word=str(input("Ingrese una palabra\n"))
    palabras.append(word)
    print("¿Desea agregar otra palabra?")
    confirmar=(input("Caso contrario, por favor, ingresar N \n"))
    pass
cuenta_letras(palabras)
