#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Se vera si la letra ingresada es vocal por medio de un if
# Caso contrario se mantendra el valor predeterminado que es false
def es_vocal(letra):
    comprobante=False
    if letra=='a' or letra =='e' or letra=='i' or letra=='o' or letra=='u':
        comprobante=True
    return comprobante

# Se contara los vocales con los espacios de la palabra
# se sumaran las vocales para tener las vocales
# Para las consonantes se borrara el total con vocales y espacios
def cuenta_vocales_consonantes(palabra):
    total=len(palabra)
    a=palabra.count('a')
    e=palabra.count('e')
    i=palabra.count('i')
    o=palabra.count('o')
    u=palabra.count('u')
    espacios=palabra.count(" ")
    vocales=a+e+i+o+u
    consonantes=total-vocales-espacios
    print("Vocales: ", vocales)
    print("Consonantes: ", consonantes)

# Se le pedira al usuario que ingrese una letra y frase
comprobar=None
letra=str(input("Ingrese una letra: "))
palabra=str(input("Ingrese la frase: "))
# La letra ingresara en comprobar, donde regresara con true o false
comprobar=es_vocal(letra)
if comprobar==True:
    print("La letra es una vocal")
    pass
elif comprobar==False:
    print("La letra es una consonante")
    pass
cuenta_vocales_consonantes(palabra)
